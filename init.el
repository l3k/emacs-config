;; add melpa repo
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

;; package installer
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Enable relative line numbers
(use-package nlinum-relative
  :ensure t
  :config
  (setq nlinum-relative-current-symbol "")
  (setq nlinum-relative-redisplay-delay 0.01)
    (add-hook 'prog-mode-hook 'nlinum-relative-mode))

;; Scroll smoothly
(setq scroll-conservatively 100)

(setq default-frame-alist
      '((width . 200)
	(height . 70)))

(setq initial-frame-alist
      '((width . 200)
	(height . 70)))

;; Disable bell alert
(setq ring-bell-function 'ignore)

;; Backup files and autosave settings
(setq make-backup-files t)
(setq auto-save-default nil)
(setq create-lockfiles nil)
(setq backup-directory-alist '(("" . "~/.emacs.d/backup")))

;; y or n
(defalias 'yes-or-no-p 'y-or-n-p)

;; Bars
(menu-bar-mode 0)
(tool-bar-mode 0)
(scroll-bar-mode 1)

;; Disable default startup screen
(setq inhibit-startup-message t)

;; Always kill current buffer
(defun kill-cur-buf ()
  (interactive)
  (kill-buffer (current-buffer)))
(global-set-key (kbd "C-x k") 'kill-cur-buf)

;; Source edit in same window
(setq org-src-window-setup 'current-window)

;; Enable line and column modeline
(line-number-mode t)
(column-number-mode t)

;; Modeline clock
(setq display-time-24hr-format t)
(display-time-mode t)

;; Font setup
(set-face-attribute 'default nil :family "Source Code Pro" :height 145)

;; Which-key (popup menu on incomplete key chords)
(use-package which-key
  :ensure t
  :init
  (which-key-mode))

;; Powerline
(use-package spaceline
  :ensure t
  :config
  (require 'spaceline-config)
  (setq powerline-default-separator (quote arrow))
  (spaceline-spacemacs-theme))

;; Dashboard
(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook)
  (setq dashboard-items '((recents . 15)))
  (setq dashboard-banner-logo-title "1nfinit3's Emacs")
  (setq dashboard-startup-banner 'logo))

;; Diminish modes
(use-package diminish
  :ensure t
  :init
  (diminish 'subword-mode)
  (diminish 'which-key-mode)
  (diminish 'eldoc-mode)
  (diminish 'page-break-lines-mode)
  (diminish 'nlinum-relative-mode))

;; Helm setup
(use-package helm
  :ensure t)

(require 'helm-config)
(global-set-key (kbd "C-x b") 'helm-buffers-list)
(global-set-key (kbd "C-x C-b") 'helm-buffers-list)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "C-x f") 'helm-find-files)
(define-key helm-map (kbd "TAB") #'helm-execute-persistent-action)
(define-key helm-map (kbd "<tab>") #'helm-execute-persistent-action)
(define-key helm-map (kbd "C-z") #'helm-select-action)

;; Org mode bullets
(use-package org-bullets
  :ensure t
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode))))

;; Subword mode
(global-subword-mode t)

;; My C-Style etc.
(use-package cc-mode
  :defer t)

(defconst my-cc-style
  '("k&r" ;; inherit from k&r style
    (c-basic-offset . 4)))

(c-add-style "1nfinit3" my-cc-style)

(setq c-default-style
      '((java-mode . "java")
	(awk-mode . "awk")
	(other . "1nfinit3")))

;; Open config file
(defun config-edit ()
  (interactive)
  (find-file "~/.emacs.d/init.el"))
(global-set-key (kbd "C-c e") 'config-edit)
(global-set-key (kbd "C-c C-e") 'config-edit)

;; Auto-completion courtasey of company and irony clang
(use-package company
  :ensure t
  :config
  (setq company-idle-delay 0)
  (setq company-minimum-prefix-length 3))

(use-package company-irony
  :ensure t
  :config
  (require 'company)
  (add-to-list 'company-backends 'company-irony))

(use-package irony
  :ensure t
  :config
  (add-hook 'c++-mode-hook 'irony-mode)
  (add-hook 'c-mode-hook 'irony-mode)
  (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options))

(use-package company-go
  :ensure t
  :config
  (require 'company)
  (add-to-list 'company-backends 'company-go))

(with-eval-after-load 'company
  (add-hook 'c++-mode-hook 'company-mode)
  (add-hook 'c-mode-hook 'company-mode)
  (add-hook 'go-mode-hook 'company-mode)
  (add-to-list 'company-backends 'company-clang))

;; Copy line without killing it C-c y
(defun copy-whole-line ()
  (interactive)
  (save-excursion
    (kill-new
     (buffer-substring
      (point-at-bol)
      (point-at-eol)))))
(global-set-key (kbd "C-c y") 'copy-whole-line)

;; Rainbow delim
(use-package rainbow-delimiters
  :ensure t
  :init
  (add-hook 'prog-mode-hook 'rainbow-delimiters-mode))

;; popup kill ring M-y
;;(use-package popup-kill-ring
;;  :ensure t
;;  :bind ("M-y" . popup-kill-ring))

;; Window splits
(defun split-and-follow-horiz ()
  (interactive)
  (split-window-below)
  (balance-windows)
  (other-window 1))
(global-set-key (kbd "C-x 2") 'split-and-follow-horiz)

(defun split-and-follow-vert ()
  (interactive)
  (split-window-right)
  (balance-windows)
  (other-window 1))
(global-set-key (kbd "C-x 3") 'split-and-follow-vert)



;; DO NOT TOUCH!
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes '(tango-dark))
 '(package-selected-packages
   '(nlinum-relative which-key use-package spaceline rainbow-delimiters org-bullets helm diminish dashboard company-irony company-go)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
